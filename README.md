# radiant-kit

Beautiful rdkit-plots

# Example usage

The intended use is for `jupyter notebooks`

```python
from radiant.plot_compounds import save_mols

smiles_list = ['c1ccccc1','Cc1occc1C(=O)Nc2ccccc2', 'CN1CCN(S(=O)(C2=CC=C(OCC)C(C3=NC4=C(N(C)N=C4CCC)C(N3)=O)=C2)=O)CC1']
mols = [Chem.MolFromSmiles(x) for x in smiles_list]
legends = ['1', '2', '3']
save_mols(mols, legends=legends, file_name='test_large.png', size=600, min_font_size=30)

```